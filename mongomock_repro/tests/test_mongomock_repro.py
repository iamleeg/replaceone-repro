import bson
import pymongo
import pytest
import mongomock
from mongomock_repro import MongoStore

@pytest.fixture
def store(monkeypatch):
    db = mongomock.MongoClient()

    def fake_mongo(connection_string):
        return db

    monkeypatch.setattr("pymongo.MongoClient", fake_mongo)
    yield MongoStore()

def test_repro(store):
    id = str(store.insert_one())
    store.replace_one(bson.ObjectId(id))
    result = store.find_one(bson.ObjectId(id))
    assert result['foo'] == 'not bar'
