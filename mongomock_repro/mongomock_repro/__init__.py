import pymongo

class MongoStore:
    def __init__(self):
        self.client = pymongo.MongoClient("mongodb://localhost:27017/db")
        self.collection = self.client["db"]["collection"]

    def insert_one(self):
        result = self.collection.insert_one({'foo': 'bar'})
        return result.inserted_id
    
    def replace_one(self, id):
        result = self.collection.replace_one({'_id': id}, {'foo': 'not bar'})
        assert result.modified_count == 1

    def find_one(self, id):
        return self.collection.find_one({'_id': id})
